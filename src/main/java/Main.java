import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random rand = new Random();
        int randX = rand.nextInt(1251); // Max # of rows = 1250.
        int randY = 2 + rand.nextInt(24); // Min # of columns = 3; Max = 25.
        int[][] table = new int[randX][randY];

        // If the first condition is false, it won't go any further.
        if (randX < 1000 & randX > 0) {
            for (int x = 0; x < randX; x++) {
                for (int y = 0; y < randY; y++) {
                    table[x][y] = rand.nextInt(10);
                    if (x == 1 & y == 2) { // Don't output the 3rd element in the 2 row of table.
                        System.out.println();
                        continue;
                    }
                    // Output every other element of the table.
                    System.out.print("[" + (x+1) + "]" + "" + "[" + (y+1) + "] = ");
                    System.out.println(table[x][y]);
                }
            }
        } else if (randX >= 1000) {
            System.out.println("It has more than thousand rows. Repeat the execution.");
        } else if (randX <= 0) {
            System.out.println("It has invalid number of rows. Repeat the execution.");
        }
    }
}
